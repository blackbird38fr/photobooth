const width = 320, 
height = 240,
webcam = document.getElementById('webcam'),
canvas = document.getElementById('photo'),
image = document.getElementById('image')
;

navigator.mediaDevices.getUserMedia({ video: true, audio: false })
            .then(function(stream){
                webcam.setAttribute('width', width);
                webcam.setAttribute('height', height);
                canvas.setAttribute('width', width);
                canvas.setAttribute('height', height);
                webcam.srcObject = stream;
                webcam.play();
            }).catch(function(err){
                console.log(err);
            });

function takePicture(){
    let context = canvas.getContext('2d');
    context.drawImage(webcam, 0, 0, width, height);
    let data = canvas.toDataURL('image/png');
    image.setAttribute('src', data);
    console.log(data);  
}

let btnTakePicture = document.getElementById('take-picture');
btnTakePicture.addEventListener('click', takePicture);
  